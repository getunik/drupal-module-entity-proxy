<?php

function entity_proxy_create_proxy()
{
	$elements['create_form'] = drupal_get_form('entity_proxy_create_form');

	return $elements;
}

function entity_proxy_create_form()
{
	$form['machine_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Machine Name'),
		'#default_value' => '',
		'#description' => t('Only use lowercase letters, digits and underscores.'),
		'#required' => TRUE,
	);

	$form['description'] = array(
		'#type' => 'textfield',
		'#title' => t('Description'),
		'#default_value' => '',
		'#description' => t('A short description of the proxy\'s purpose'),
		'#required' => TRUE,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#title' => t('Create'),
		'#value' => 'create',
	);

	return $form;
}

function entity_proxy_create_form_validate($form, &$form_state)
{
}

function entity_proxy_create_form_submit($form, &$form_state)
{
	$proxy = entity_create('entity_proxy', array());// new EntityProxy();
	$proxy->machine_name = $form_state['values']['machine_name'];
	$proxy->description = $form_state['values']['description'];
	$proxy->config = new EntityProxyConfig();
	$proxy->status = ENTITY_CUSTOM;
	$proxy->module = NULL;

	$proxy->save();

	drupal_goto('admin/structure/entity_proxy');
}

function entity_proxy_admin_list_all()
{
	$proxies = EntityProxy::loadAll();
	$elements = array();

	$rows = array();

	foreach ($proxies as $proxy)
	{
		$rows[] = array(
			$proxy->machine_name,
			'unknown',
			// TODO: move URL to entity object
			l('Target', '/admin/structure/entity_proxy/target/' . $proxy->machine_name),
		);
	}

	$elements['proxies'] = array(
		'#theme' => 'table',
		'#header' => array(
			t('Name'),
			t('Status'),
			t('Actions'),
		),
		'#rows' => $rows,
	);

	return $elements;
}

function entity_proxy_admin_list_targets(EntityProxy $proxy)
{
	$elements = array();
	$rows = array();

	foreach ($proxy->getReferences(array('ignoreNulls' => FALSE, 'allLanguages' => TRUE)) as $reference)
	{
		$rows[] = array(
			'NULL',
			(string)$reference,
			'unknown',
			// TODO: move URL to entity object
			l('Remove', '/admin/structure/entity_proxy/target/' . $proxy->machine_name . '/delete/' . $reference->id),
		);
	}

	$elements['references'] = array(
		'#theme' => 'table',
		'#header' => array(
			t('Title'),
			t('Info'),
			t('Status'),
			t('Actions'),
		),
		'#rows' => $rows,
	);

	return $elements;
}

function entity_proxy_admin_create_target(EntityProxy $proxy)
{
	$elements['create_target_form'] = drupal_get_form('entity_proxy_create_target_form', $proxy);

	return $elements;
}

function entity_proxy_create_target_form($form, &$form_state, $proxy)
{
	$form['ref_language'] = array(
		'#type' => 'textfield',
		'#title' => t('Language'),
		'#default_value' => 'und',
		'#description' => t('Target language'),
		'#required' => TRUE,
	);

	$form['ref_target_entity_type'] = array(
		'#type' => 'textfield',
		'#title' => t('Entity Type'),
		'#default_value' => '',
		'#description' => t('Target entity type'),
		'#required' => TRUE,
	);

	$form['ref_target_entity_bundle'] = array(
		'#type' => 'textfield',
		'#title' => t('Bundle Type'),
		'#default_value' => '',
		'#description' => t('Target bundle type'),
		'#required' => FALSE,
	);

	$form['ref_target_entity_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Entity ID'),
		'#default_value' => '',
		'#description' => t('Target entity ID'),
		'#required' => TRUE,
	);

	$form['ref_target_entity_vid'] = array(
		'#type' => 'textfield',
		'#title' => t('Revision ID'),
		'#default_value' => '',
		'#description' => t('Target entity revision ID'),
		'#required' => FALSE,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#title' => t('Create'),
		'#value' => 'create',
	);

	return $form;
}

function entity_proxy_create_target_form_validate($form, &$form_state)
{
}

function entity_proxy_create_target_form_submit($form, &$form_state)
{
	$proxy = reset($form_state['build_info']['args']);
	$ref = new EntityProxyReference($proxy);
	$ref->proxy = $proxy->machine_name;

	foreach ($form_state['values'] as $key => $value)
	{
		if (preg_match('/^ref_/', $key))
		{
			$ref->{substr($key, 4)} = empty($value) ? NULL : $value;
		}
	}

	$info = entity_get_info($ref->target_entity_type);

	$id_prop = $info['entity keys']['id'];

	$target = entity_load($ref->target_entity_type, array($ref->target_entity_id));
	$target = reset($target);

	$ref->target_entity_bundle = $target->{$info['bundle keys']['bundle']};

	$ref->save();

	drupal_goto('admin/structure/entity_proxy/target/' . $proxy->machine_name);
}

function entity_proxy_admin_delete_target(EntityProxy $proxy, $target_id)
{
	foreach ($proxy->getReferences(array('ignoreNulls' => FALSE, 'allLanguages' => TRUE)) as $ref)
	{
		if ($ref->id == $target_id)
		{
			$ref->delete();
		}
	}

	drupal_goto('admin/structure/entity_proxy/target/' . $proxy->machine_name);
}
