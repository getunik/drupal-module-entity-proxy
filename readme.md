# Introduction
The Entity Proxy is an exportable entity type with a human readable machine name that can be used to refer to an arbitrary number of arbitrary entities. It is meant to bridge the gap between code and content for cases where code needs to explicitly refer to content elements (entities). Instead of directly referencing (hard-coding) an entity ID or GUID, with entity proxies, it is possible to refer to the well-defined proxy instead and let that one worry about what entities it points to.

**Warning:** The module is quite incomplete in terms of UI and user friendly-ness. The API  as documented here is supposed to be fairly stable, nevertheless, you should use this version with caution.

## Theme Hook Suggestions
* `entity`
* `entity_proxy`
* `entity_proxy__[machine_name]`

# Examples

## Declare a Proxy in Code
```php
function hook_default_entity_proxy()
{
	$proxy = entity_create('entity_proxy', array());
	$proxy->machine_name = 'my_proxy_name';
	$proxy->description = 'This is a proxy defined in code';
	$proxy->config = new EntityProxyConfig();

	$defaults = array(
		$proxy->machine_name => $proxy,
	);

	return $defaults;
}
```

## Getting a Proxy in Code
All you need to fetch a proxy is its machine name, the rest should be fairly obvious

```php
$proxy = EntityProxy::load('my_proxy');
// get the render array for the proxy
$render_array = $proxy->view();
// just render everything referenced by the proxy
print $proxy->render();
```

## Working with References

```php
// this will only get the reference items that point to an existing entity
$items = $proxy->getReferences();
// this will get _all_ references, including broken ones
$items = $proxy->getReferences(array('ignoreNulls' => FALSE));
// gets the first (existing) reference of the proxy (NULL if none exists)
$first = $proxy->first();

// get the entity metadata wrapper referenced by a proxy
$target = $proxy->first()->get();
```

## Theming of a Specific Proxy
The usual theme hook suggestions for entities apply. A custom template can easily be added to the theme
```html
<div class="<?php print $classes; ?>"<?php print $attributes; ?> <?php print $content_attributes; ?>>
    <?php print render($content); ?>
</div>
```

To add some markup around the proxied entities, you can do something like this
```html
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> <?php print $content_attributes; ?>>
	<?php foreach (element_children($content) as $child): ?>
		<div class="some custom container">
			<span class="before">BEFORE</span>
			<?php print render($content[$child]); ?>
			<span class="before">AFTER</span>
		</div>
	<?php endforeach; ?>
</div>

```
