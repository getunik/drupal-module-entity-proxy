<?php

$plugin = array(
	'title' => t('Entity Proxy'), // Title to show up on the pane screen.
	'description' => t('Entity Proxy'), // Description to show up on the pane screen.
	'category' => t('Entity'), // A category to put this under.
	'defaults' => array( // Array of defaults for the settings form.
		'proxy_name' => '',
		'view_mode' => 'full',
	),
	'all contexts' => TRUE, // This is NEEDED to be able to use substitution strings in your pane.
);


/**
 * {module}_{plugin-name}_content_type_edit_form
 */
function entity_proxy_entity_proxy_content_type_edit_form($form, &$form_state)
{
	$conf = $form_state['conf'];

	$options = array();
	$all_proxies = EntityProxy::loadAll();
	foreach ($all_proxies as $proxy)
	{
		$options[$proxy->machine_name] = $proxy->machine_name . ' (' . $proxy->description . ')';
	}


	$form['proxy_name'] = array(
		'#type' => 'select',
		'#title' => t('Proxy'),
		'#description' => t('The proxy to render'),
		'#default_value' => $conf['proxy_name'],
		'#options' => $options,
	);

	$modes = array();
	$all_infos = entity_get_info();
	foreach ($all_infos as $entity_type => $entity_info)
	{
		foreach ($entity_info['view modes'] as $mode => $settings)
		{
			if (!isset($modes[$mode]))
			{
				$modes[$mode] = array(
					'label' => $settings['label'],
					'types' => array(),
				);
			}

			$modes[$mode]['types'][] = $entity_type;
		}
	}

	$options = array_map(function ($m) { return $m['label'] . ' (' . implode(', ', $m['types']) . ')'; }, $modes);

	$form['view_mode'] = array(
		'#type' => 'select',
		'#title' => t('View Mode'),
		'#description' => t('The view mode to use'),
		'#default_value' => $conf['view_mode'],
		'#options' => $options,
	);

	return $form;
}

/**
 * {module}_{plugin-name}_content_type_edit_form_submit
 */
function entity_proxy_entity_proxy_content_type_edit_form_submit(&$form, &$form_state)
{
	foreach (array('proxy_name', 'view_mode') as $key) {
		$form_state['conf'][$key] = $form_state['values'][$key];
	}
}

/**
 * {module}_{plugin-name}_content_type_render
 */
function entity_proxy_entity_proxy_content_type_render($subtype, $conf, $args, $contexts)
{
	$proxy = EntityProxy::load($conf['proxy_name']);

	$block = new stdClass();
	$block->title = '';
	$block->content = $proxy->view($conf['view_mode']);

	return $block;
}

/**
 * {module}_{plugin-name}_content_type_admin_info
 */
function entity_proxy_entity_proxy_content_type_admin_info($subtype, $conf, $contexts)
{
	if (!empty($conf))
	{
		$block = new stdClass;
		$block->title = 'Entity Proxy';
		$block->content =  t('@name with view mode @mode', array('@name' => $conf['proxy_name'], '@mode' => $conf['view_mode']));
		return $block;
	}
}
