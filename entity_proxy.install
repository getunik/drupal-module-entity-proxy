<?php

function entity_proxy_schema()
{
	$schema['entity_proxy'] = array(
		'description' => t('The base table for entity proxies.'),
		'fields' => array(
			'machine_name' => array(
				'description' => 'The primary human readable identifier for a proxy.',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
			),
			'description' => array(
				'description' => 'Description of the intended purpose of the entity proxy.',
				'type' => 'text',
				'not null' => FALSE,
			),
			'config' => array(
				'description' => 'The proxy configuration (serialized PHP)',
				'type' => 'blob',
				// the serialize will use PHP serialization to store arbitrary PHP objects
				'serialize' => TRUE,
				'size' => 'normal',
				'not null' => TRUE,
			),
			// required fields for exportables
			'status' => array(
				'type' => 'int',
				'not null' => TRUE,
				// Set the default to ENTITY_CUSTOM without using the constant as it is
				// not safe to use it at this point.
				'default' => 0x01,
				'size' => 'tiny',
				'description' => 'The exportable status of the entity.',
			),
			'module' => array(
				'description' => 'The name of the providing module if the entity has been defined in code.',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
			),
		),
		'unique keys' => array(
			'unique_machine_name' => array('machine_name'),
		),
		'primary key' => array('machine_name'),
	);

	$schema['entity_proxy_reference'] = array(
		'description' => t('The entity proxy outgoing reference table.'),
		'fields' => array(
			'id' => array(
				'description' => 'The primary identifier for a proxy target reference.',
				'type' => 'serial',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'proxy' => array(
				'description' => 'The primary human readable identifier for a proxy.',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
			),
			'language' => array(
				'description' => 'The language of the proxy reference.',
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
			),
			'target_entity_type' => array(
				'description' => '',
				'type' => 'varchar',
				'length' => 128,
				'not null' => TRUE,
			),
			'target_entity_bundle' => array(
				'description' => '',
				'type' => 'varchar',
				'length' => 128,
				'not null' => TRUE,
			),
			'target_entity_id' => array(
				'description' => '',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'target_entity_vid' => array(
				'description' => '',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => FALSE,
			),
			'weight' => array(
				'description' => '',
				'type' => 'int',
				'unsigned' => FALSE,
				'not null' => FALSE,
			),
		),
		'indexes' => array(
			'proxy_reference' => array('proxy'),
		),
		'primary key' => array('id'),
	);

	return $schema;
}
