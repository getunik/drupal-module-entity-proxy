<?php

/**
 * Entity proxy configuration object
 */
class EntityProxyConfig
{
	private $allowedTargets;
	private $languageMode;

	public function __construct()
	{
		$this->allowedTargets = array();
		$this->languageMode = 'default';
	}

	public function getLanguageMode()
	{
		return $this->languageMode;
	}
}
