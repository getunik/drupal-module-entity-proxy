<?php

/**
 * Entity proxy reference object
 */
class EntityProxyReference
{
	const TABLE_NAME = 'entity_proxy_reference';

	protected $entity;
	protected $is_null;

	public $parent;

	public $id;
	public $proxy;
	public $language;
	public $target_entity_type;
	public $target_entity_bundle;
	public $target_entity_id;
	public $target_entity_vid;
	public $weight;

	public function __construct(EntityProxy $parent, $dbrow = NULL)
	{
		$this->parent = $parent;
		if (!empty($dbrow))
		{
			foreach (get_object_vars($dbrow) as $key => $value)
			{
				if (property_exists($this, $key))
				{
					$this->{$key} = $value;
				}
			}
		}
	}

	public function __toString()
	{
		return implode(':', array($this->target_entity_type, $this->target_entity_bundle, $this->target_entity_id));
	}

	public function save()
	{
		if (isset($this->id))
		{
			$query = db_update(self::TABLE_NAME);
			$query->condition('id', $this->id);
		}
		else
		{
			$query = db_insert(self::TABLE_NAME);
		}

		$query->fields($this->toFields());
		$query->execute();
	}

	public function delete()
	{
		if (isset($this->id))
		{
			$query = db_delete(self::TABLE_NAME);
			$query->condition('id', $this->id);
			$query->execute();
		}
	}

	public function get()
	{
		if (empty($this->entity) && !$this->is_null)
		{
			$target = entity_load($this->target_entity_type, array($this->target_entity_id));
			if (empty($target))
			{
				$this->is_null = TRUE;
			}
			else
			{
				$target = reset($target);
				$this->entity = entity_metadata_wrapper($this->target_entity_type, $target);
				$this->is_null = FALSE;
			}
		}

		return $this->is_null ? NULL : $this->entity;
	}

	public function isNull()
	{
		return $this->get() === NULL;
	}

	protected function toFields()
	{
		return array(
			'proxy' => $this->proxy,
			'language' => $this->language,
			'target_entity_type' => $this->target_entity_type,
			'target_entity_bundle' => $this->target_entity_bundle,
			'target_entity_id' => $this->target_entity_id,
			'target_entity_vid' => $this->target_entity_vid,
			'weight' => $this->weight,
		);
	}

	public function view($viewMode = 'full', $langcode = NULL)
	{
		$target = $this->get();
		if ($target !== NULL)
		{
			return $target->view($viewMode, $langcode);
		}
	}

	public function render($viewMode = 'full', $langcode = NULL)
	{
		$tmp = $this->view($viewMode, $langcode);
		return render($tmp);
	}

	public static function getForProxy(EntityProxy $proxy)
	{
		$references = array();
		$query = db_select(self::TABLE_NAME);
		$rows = $query
			->fields(self::TABLE_NAME)
			->condition('proxy', $proxy->machine_name)
			->execute()
			->fetchAll();

		foreach ($rows as $row)
		{
			$references[] = new EntityProxyReference($proxy, $row);
		}

		return $references;
	}
}
