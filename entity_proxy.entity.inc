<?php

/**
 * Extending the EntityAPIControllerExportable for entity proxy entity.
 */
class EntityProxyController extends EntityAPIControllerExportable
{
	public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array())
	{
		$result = array();

		foreach ($entity->getReferences() as $ref)
		{
			$result[] = $ref->view($view_mode, $langcode);
		}

		return $result;
	}
}


/**
 * Extending the Entity for the entity proxy entity.
 */
class EntityProxy extends Entity
{
	protected $references;

	public function __construct(array $values = array())
	{
		parent::__construct($values, 'entity_proxy');
		$this->references;
	}

	public function getReferences($options = array())
	{
		global $language_content;

		if (!isset($this->references))
		{
			$this->references = EntityProxyReference::getForProxy($this);
		}

		$result = $this->references;
		$options = array_merge(array('ignoreNulls' => TRUE, 'allLanguages' => FALSE), $options);
		$languages = array();

		if (!$options['allLanguages'])
		{
			if ($this->config->getLanguageMode() === 'default')
			{
				$languages[LANGUAGE_NONE] = TRUE;
				$languages[$language_content->language] = TRUE;
			}
		}

		$result = array_filter($result, function ($ref) use ($options, $languages) {
			if (!$options['ignoreNulls'] && $ref->isNull())
				return FALSE;

			if (!empty($languages) && !isset($languages[$ref->language]))
				return FALSE;

			return TRUE;
		});

		return $result;
	}

	public function first($options = array())
	{
		$all = $this->getReferences($options);
		return reset($all);
	}

	public function firstEntity($options = array())
	{
		$ref = $this->first($options);
		return $ref ? $ref->get() : $ref;
	}

	public function render($viewMode = 'full', $langcode = NULL)
	{
		$tmp = $this->view($viewMode, $langcode);
		return render($tmp);
	}

	public static function load($name)
	{
		$entities = entity_load('entity_proxy', array($name));
		$proxy = reset($entities);
		return $proxy;
	}

	public static function loadAll()
	{
		return entity_load('entity_proxy');
	}
}
